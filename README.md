# Docker PostgreSQL and Data Backup

Docker PostgreSQL and Data Backup simplifies backing up an dockerized applications with PostgreSQL using [restic](https://restic.net/),  [pg_dump](https://www.postgresql.org/docs/9.6/app-pgdump.html) and [supercronic](https://github.com/aptible/supercronic).


## Features
- Scheluled backups
- Restore functionality
- Various [backends](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html) are supported


## Usage
See [docker-compose.yaml](docker-compose.yaml)

### Backup
The application and PostgreSQL container have to be running!
For a instant backup execute <code>docker exec -it *container-name* /usr/local/bin/entrypoint.sh backup</code>.

### Show snapshots
To see all snapshots just execute <code>docker exec -it *container-name* /usr/local/bin/entrypoint.sh snapshots</code>.

### Restore
1. Stop PostgreSQL, application and all other containers regarding the application
2. Ensure that all application volumes are empty (PostgreSQL and other volumes regarding the application too)
3. Start the database and the backup container
5. Execute <code>docker exec -it *container-name* /usr/local/bin/entrypoint.sh snapshots</code> and choose a snapshot to restore
6. Execute <code>docker exec -it *container-name* /usr/local/bin/entrypoint.sh restore {snapshot id}</code>
7. Restart all containers except the backup container

### Connect to database
To connect to the database using psql just execute <code>docker exec -it *container-name* /usr/local/bin/entrypoint.sh psql</code>.

## Commands
- start - Initiate the restic repository and start the supercronic to execute periodic backups
- backup - Create instant backup of the database and data in the /data directory
- restore - Restore Database and data to the /data directory (Database and /data directory must be empty before execution)
- snapshots - Print a list of all snapshots from the restic repository
- psql - Open psql shell within the database

## ENV variables:
| Variable          | Default       |                                         |
|-------------------|---------------|-----------------------------------------|
| DB_HOST           | database      | PostgreSQL host                         |
| DB_USER           | root          | User for PostgreSQL                     |
| DB_PASSWORD       | root          | Password for  PostgreSQL                |
| DB_NAME           | database      | Database name                           |
| RESTIC_REPOSITORY | /backup       | Restic repository path                  |
| RESTIC_PASSWORD   | passw0rd      | Restic repository password              |
| DATA_DIR          | /data         | Application data directory              |
| CRON              | "0 22 * * 6"  | Crontab execution time                  |
| TZ                | Europe/Berlin | Local timezone                          |
| ...               |               | [More](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html) to configure the restic repository |
