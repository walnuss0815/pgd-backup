FROM restic/restic:0.9.5

ENV DB_HOST=database \
    DB_USER=root \
    DB_PASSWORD=root \
    DB_NAME=database \
    RESTIC_REPOSITORY=/backup \
    RESTIC_PASSWORD=passw0rd \
    DATA_DIR=/data \
    CRON="0 22 * * 6" \
    SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.8/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=be43e64c45acd6ec4fce5831e03759c89676a0ea \
    TZ=Europe/Berlin

RUN apk add --update --no-cache postgresql curl \
 && curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

ADD entrypoint.sh /usr/local/bin/entrypoint.sh

VOLUME [ "/data" ]

ENTRYPOINT [ "entrypoint.sh" ]

CMD [ "start" ]