#!/bin/sh

start() {
    ## Create repo if it doesn't exist
    restic snapshots --repo $RESTIC_REPOSITORY || restic init --repo $RESTIC_REPOSITORY

    ## Start cron
    echo "$CRON entrypoint.sh backup">crontab
    supercronic crontab
}

backup() {
    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## Database backup
    pg_dump -h $DB_HOST -U $DB_USER -c $DB_NAME -w -C > "$DATA_DIR/backup.sql"

    ## File backup
    restic -r $RESTIC_REPOSITORY --verbose backup $DATA_DIR
}

restore() {
    ## Check if snapshot supplied
    if [ -z "$1" ]
    then
        echo "No snapshot supplied"
        exit 1
    fi
    SNAPSHOT=$1

    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## File restore
    restic --repo $RESTIC_REPOSITORY restore $SNAPSHOT --target $DATA_DIR/..

    ## Database restore
    psql --set ON_ERROR_STOP=on -h $DB_HOST -U $DB_USER postgres -w < "$DATA_DIR/backup.sql"
}

snapshots() {
    ## Print snapshots
    restic snapshots --repo $RESTIC_REPOSITORY 
}

psql() {
    ## Set database password
    export PGPASSWORD=$DB_PASSWORD

    ## Connecto to database
    psql -h $DB_HOST -U $DB_USER $DB_NAME -w
}


ACTION=$1

echo "PostgreSQL and Data Backup: $ACTION"

case "$ACTION" in
	start)
		start
        break
		;;
	backup)
		backup
		break
		;;
	restore)
		restore $2
		break
		;;
    snapshots)
		snapshots
		break
		;;
    psql)
		psql
		break
		;;
	*)
		echo "Sorry, I don't understand"
		;;
esac